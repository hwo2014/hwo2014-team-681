#!/usr/bin/env python

import numpy as np
from sklearn import linear_model
from sklearn import svm

def prepare_vel(data):
    throttle = data[:,0]
    vel = data[:,3]
    accel = data[:,4]
    carAngle = data[:,1]
    curveAngle = data[:,6]
    curveRadius = data[:,7]
    turbo = data[:,8]
    
    y = vel[1:] # Looking for next velocities
    vel = vel[:-1]
    throttle = throttle[:-1]
    turbo = turbo[:-1]
    curveAngle = curveAngle[:-1]
    curveRadius = curveRadius[:-1]
    carAngle = carAngle[:-1]

    X = np.column_stack(( vel,vel**3, np.multiply(throttle,turbo)  ))

    return X,y

def process_vel(data,test_data):
    print("Velocity: ")

    X,accel = prepare_vel(data) 

    clf = linear_model.LinearRegression()
    clf.fit(X,accel)

    print("%s %s" % (str(clf.coef_), str(clf.intercept_)))

    X_test,accel_test = prepare_vel(test_data)

    # Adjust data for variable physics

    residual_error = np.mean((clf.predict(X_test) - accel_test)**2)
    mean_error = np.mean((clf.predict(X_test) - accel_test))

    print("Residual error: %f" % residual_error)
    print("Mean error: %f" % mean_error)    # Just making sure...
    print("Score: %f" % clf.score(X_test,accel_test))

    residual_error = np.mean((clf.predict(X_test) - mean_error - accel_test))
    print("Adjusted residual error: %f" % residual_error)

    clf.intercept_ = clf.intercept_ - mean_error

    # Check fits (Cheating by fitting to ourselves...)
    print("***Adjusted Score: %f" % clf.score(X_test,accel_test))
    
    errors = np.abs((clf.predict(X_test) - accel_test))

    largest_error = errors[0]
    largest_error_i = 0

    for i in range(1,len(errors)):
        if errors[i] > largest_error:
            largest_error = errors[i]
            largest_error_i = i

    print("Largest error: %f at %d" % (largest_error,largest_error_i))


def prepare(data):
    throttle = data[:,0]
    vel = data[:,3]
    accel = data[:,4]
    carAngle = data[:,1]
    curveAngle = data[:,6]
    curveRadius = data[:,7]
    turbo = data[:,8]

    # Adjust accel to match previous colum
    accel = accel[1:]

    # Drop last throttle / vel / carAngle / curveAngle
    throttle = throttle[:-1]
    vel = vel[:-1]
    carAngle = carAngle[:-1]
    curveAngle = curveAngle[:-1]
    curveRadius = curveRadius[:-1]
    turbo = turbo[:-1]
    
    centriForce = np.zeros(len(curveRadius))

    for i in range(0,len(curveRadius)):
        if curveRadius[i] != 0:
            centriForce[i] = vel[i]**2 / curveRadius[i]

    # Try a few other paramaaters

#    X = np.column_stack(( throttle**2,vel,vel**2,np.multiply(vel,np.tan(carAngle)),np.multiply(throttle,np.tan(carAngle)),np.multiply(throttle,vel,np.tan(carAngle)),np.multiply(throttle**2,turbo),np.multiply(throttle,turbo,np.tan(carAngle)),centriForce ))

#    X = np.column_stack(( throttle, vel**2, np.multiply(vel, np.cos(carAngle)), np.multiply(throttle,np.cos(carAngle)), np.multiply(throttle, turbo)  ))
#    X = np.column_stack(( np.multiply(throttle,turbo), vel, vel**2, np.multiply(curveAngle,curveRadius), np.multiply(curveAngle,vel), np.multiply(carAngle,vel), 
#         np.multiply(throttle,vel)  ))
#    X = np.column_stack(( throttle, vel**2, np.exp(throttle), np.exp(vel),  np.multiply(curveAngle,curveRadius)*np.pi/180, np.multiply(carAngle,throttle) ))

    dthrottle = np.zeros(throttle.shape)
    dthrottle[0] = throttle[0]
    for i in range(1,len(throttle)):
        dthrottle[i] = throttle[i] - throttle[i-1]

    #X = np.column_stack((np.multiply(throttle,turbo), vel**2,  np.sqrt(throttle), np.sqrt(vel), throttle**2, np.multiply(throttle,curveAngle), np.multiply(vel,curveAngle),
    #    curveRadius, curveRadius*curveAngle, np.multiply(throttle,carAngle), np.multiply(vel,carAngle) ))

    X = np.column_stack(( throttle, vel, turbo ))

    return X,accel


def process(data,test_data):
    X,accel = prepare(data) 

    clf = linear_model.LinearRegression()
    clf.fit(X,accel)

    print("%s %s" % (str(clf.coef_), str(clf.intercept_)))

    X_test,accel_test = prepare(test_data)

    # Adjust data for variable physics

    residual_error = np.mean((clf.predict(X_test) - accel_test)**2)
    mean_error = np.mean((clf.predict(X_test) - accel_test))

    print("Residual error: %f" % residual_error)
    print("Mean error: %f" % mean_error)    # Just making sure...
    print("Score: %f" % clf.score(X_test,accel_test))

    residual_error = np.mean((clf.predict(X_test) - mean_error - accel_test))
    print("Adjusted residual error: %f" % residual_error)

    clf.intercept_ = clf.intercept_ - mean_error

    # Check fits (Cheating by fitting to ourselves...)
    print("***Adjusted Score: %f" % clf.score(X_test,accel_test))

    # Try an SVM to see...

    #clf = svm.SVR()

    #clf.fit(X,accel)

    #residual_error = np.mean((clf.predict(X_test) - accel_test)**2)

    #print("Residual error: %s" % str(residual_error))

    #print("Score: %f" % clf.score(X_test,accel_test))


# Grab the data up to the first fall off (if any)

data = np.genfromtxt("../stats",delimiter=",")
test_data = np.genfromtxt("../stats2",delimiter=",")

for i in range(0,len(data)):
    if data[i,-1] == 1:
        break

data = data[:i,:]

for i in range(0,len(test_data)):
    if test_data[i,-1] == 1:
        break

test_data = test_data[:i,:]

print("Data size: %s" % str(data.shape))
print("Test data size: %s" % str(test_data.shape))

process(data,test_data)
process_vel(data,test_data)

largest_change = 0
change_i = 1

for i in range(1,len(data)):
    change = np.abs(data[i,4] - data[i-1,4])
    if change > largest_change:
        largest_change = change
        change_i = i

print("Largest change in acceleration: %f at %d" % (largest_change,change_i))

largest_change = 0
change_i = 1

for i in range(1,len(test_data)):
    change = np.abs(test_data[i,4] - test_data[i-1,4])
    if change > largest_change:
        largest_change = change
        change_i = i

print("Largest change in acceleration: %f at %d" % (largest_change,change_i))


