package jarramcintyre.hwopenbot;

import jarramcintyre.hwopenbot.messages.JsonUtil;
import jarramcintyre.hwopenbot.messages.MsgWrapper;

import java.util.AbstractMap;
import java.util.ArrayList;

public class Race {
	private String id;
	private String name;
	
	private boolean isQualifyingRound = false;
	
	private double duration_ms;
	
	private int laps;
	private double maxLapTime_ms;
	private boolean quickRace;
	
	final private ArrayList<TrackPiece> track = new ArrayList<TrackPiece>();
	final private ArrayList<Double> lanes = new ArrayList<Double>();
	
	private Car me;
	final private ArrayList<Car> cars = new ArrayList<Car>();
	
	private boolean haveTurbo = false;
	private int remainingTurboTicks = 0; 
	private double turboDurationMs;
	private int turboDurationTicks;
	private double turboFactor;
	
	private double lastThrottle = 0.0;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Race(String myName, String myColor, MsgWrapper initMsg) {
		id = (String)initMsg.getValue("race:track:id");
		name = (String)initMsg.getValue("race:track:name");
		
		if(initMsg.getValue("race:raceSession:durationMs") != null) {
			isQualifyingRound = true;
			duration_ms = ((Double)initMsg.getValue("race:raceSession:durationMs")).doubleValue();
		}
		else {
			isQualifyingRound = false;
			laps = ((Double)initMsg.getValue("race:raceSession:laps")).intValue();
			maxLapTime_ms = ((Double)initMsg.getValue("race:raceSession:maxLapTimeMs")).doubleValue();
			quickRace = ((Boolean)initMsg.getValue("race:raceSession:quickRace")).booleanValue();
		}
		
		for(Object p : (ArrayList)initMsg.getValue("race:track:pieces")) {
			track.add(TrackPiece.createPiece((AbstractMap<String,?>)p));
		}
		
		for(AbstractMap<String,Double> l : (ArrayList<AbstractMap<String,Double>>)initMsg.getValue("race:track:lanes")) {
			lanes.add(l.get("distanceFromCenter"));
		}
		
		for(Object c : (ArrayList)initMsg.getValue("race:cars")) {
			final Car nc = new Car(this,(AbstractMap<String,?>)c, myColor); 
			cars.add(nc);
			
			if(nc.isMe()) {
				assert(me == null);
				me = nc;
			}
		}
		
		assert(me != null);
		
	}
	
	public int trackHash() {
		return track.hashCode();
	}
	
	@SuppressWarnings("unchecked")
	public void updateCars(MsgWrapper msg) {
		for(AbstractMap<String,?> c : (ArrayList<AbstractMap<String,?>>)msg.data) {
			final String color = (String)JsonUtil.getValue(c, "id:color");
			
			for(Car car : cars) {
				if(car.getColor().equals(color)) {
					car.update(c);
				}
			}
		}
	}
	
	public Car updateCrash(MsgWrapper msg) {
		final String color = (String)msg.getValue("color");
		
		for(Car car : cars) {
			if(car.getColor().equals(color)) {
				car.setCrashed(true);
				return car;
			}
		}
		
		return null; // Should not reach here!
	}
	
	public void updateSpawn(MsgWrapper msg) {
		final String color = (String)msg.getValue("color");
		
		for(Car car : cars) {
			if(car.getColor().equals(color)) {
				car.setCrashed(false);
			}
		}
	}
	
	// Car was disqualified...
	public void updateDnf(MsgWrapper msg) {
		final String color = (String)msg.getValue("car:color");
		
		for(Car car : cars) {
			if(car.getColor().equals(color)) {
				car.setDisqualified(true);
			}
		}
	}
	
	public void updateTurbo(MsgWrapper msg) {
		turboDurationMs = ((Double)msg.getValue("turboDurationMilliseconds")).doubleValue();
		turboDurationTicks = ((Double)msg.getValue("turboDurationTicks")).intValue();
		turboFactor = ((Double)msg.getValue("turboFactor")).doubleValue();
		haveTurbo = true;
	}
	
	public void tickTurbo() {
		remainingTurboTicks = Math.max(0, remainingTurboTicks-1);
	}
	
	public boolean haveTurbo() {
		return haveTurbo;
	}
	
	public void useTurbo() {
		if(haveTurbo) {
			remainingTurboTicks = turboDurationTicks;
			haveTurbo = false;
		}
	}
	
	public boolean usingTurbo() {
		return remainingTurboTicks != 0;
	}

	// Autogen:
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isQualifyingRound() {
		return isQualifyingRound;
	}

	public double getDuration_ms() {
		return duration_ms;
	}

	public int getLaps() {
		return laps;
	}

	public double getMaxLapTime_ms() {
		return maxLapTime_ms;
	}

	public boolean isQuickRace() {
		return quickRace;
	}

	public ArrayList<TrackPiece> getTrack() {
		return track;
	}
	
	public ArrayList<Double> getLanes() {
		return lanes;
	}
	
	public Car getMe() {
		return me;
	}

	public ArrayList<Car> getCars() {
		return cars;
	}
	
	public double getTurboDurationMs() {
		return turboDurationMs;
	}

	public double getTurboDurationTicks() {
		return turboDurationTicks;
	}

	public double getTurboFactor() {
		return turboFactor;
	}
	
	public double getLastThrottle() {
		return lastThrottle;
	}

	public void setLastThrottle(double lastThrottle) {
		this.lastThrottle = lastThrottle;
	}

	@Override
	public String toString() {
		return "Race [id=" + id + ", name=" + name + ", isQualifyingRound="
				+ isQualifyingRound + ", duration_ms=" + duration_ms
				+ ", laps=" + laps + ", maxLapTime_ms=" + maxLapTime_ms
				+ ", quickRace=" + quickRace + ", track=" + track + ", lanes="
				+ lanes + ", me=" + me + ", cars=" + cars + ", haveTurbo="
				+ haveTurbo + ", turboDurationMs=" + turboDurationMs
				+ ", turboDurationTicks=" + turboDurationTicks
				+ ", turboFactor=" + turboFactor + ", lastAcceleration="
				+ lastThrottle + "]";
	}
}