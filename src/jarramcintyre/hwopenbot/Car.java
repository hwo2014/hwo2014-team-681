package jarramcintyre.hwopenbot;

import java.util.AbstractMap;

import jarramcintyre.hwopenbot.messages.JsonUtil;

public class Car {
	private Race race;
	
	// *** Game values:
	
	private String name;
	private String color;
	private double length;
	private double width;
	private double guideFlagPosition;
	private boolean me;
	
	private double angle = 0.0;
	private int pieceIndex = 0;
	private double inPieceDistance = 0.0;
	private int startLane = 0;
	private int endLane = 0;
	private int lap = 0;
	
	private boolean crashed = false;
	
	private boolean disqualified = false;
	
	// *** Stats:
	
	// Velocity values
	private final double laneAdjust = 2.06; // Extra distance for lane changes (not 100% accurate)
		
	private boolean changedLanes = false;
	private double lastLaneOffset = 0.0;
	
	private int lastPieceIndex = 0;
	private double lastDistance = 0.0;
		
	private double velocity = 0.0;
	private double acceleration = 0.0;
	
	// Car angle values
	private double angle_1 = 0.0; // Angle(t-1) = car angle (not absolute)
	
	private double theta = 0.0; // Theta(t) = car angle 
	private double dtheta = 0.0; // DeltaTheta(t)
	private double theta_1 = 0.0; // Theta(t-1)
	private double theta_2 = 0.0; // Theta(t-2)
	private double dtheta_1 = 0.0;  // DeltaTheta(t-1)
	
	// Protocol Related Stuff:
	
	public Car(Race race, AbstractMap<String,?> data, String myColor) {
		this.race = race;
		
		name = (String)JsonUtil.getValue(data, "id:name");
		color = (String)JsonUtil.getValue(data, "id:color");
		
		me = color.equals(myColor);
		
		length = ((Double)JsonUtil.getValue(data, "dimensions:length")).doubleValue();
		width = ((Double)JsonUtil.getValue(data, "dimensions:width")).doubleValue();
		guideFlagPosition = ((Double)JsonUtil.getValue(data, "dimensions:guideFlagPosition")).doubleValue();
	}
	
	public void update(AbstractMap<String,?> data) {
		angle = ((Double)data.get("angle")).doubleValue();
		pieceIndex = ((Double)JsonUtil.getValue(data, "piecePosition:pieceIndex")).intValue();
		inPieceDistance = ((Double)JsonUtil.getValue(data, "piecePosition:inPieceDistance")).doubleValue();
		startLane = ((Double)JsonUtil.getValue(data, "piecePosition:lane:startLaneIndex")).intValue();
		endLane = ((Double)JsonUtil.getValue(data, "piecePosition:lane:endLaneIndex")).intValue();
		lap = ((Double)JsonUtil.getValue(data, "piecePosition:lap")).intValue();
		
		updateStats();
	}
	
	// Stats Handling:
	
	private boolean firstTurn = true;
	
	private void updateStats() {
		final double laneOffset = race.getLanes().get(startLane);
		
		if(firstTurn) {
			// Need to wait for the first update() to get these values.
			lastLaneOffset = laneOffset;
			lastPieceIndex = pieceIndex;
			
			firstTurn = false;
		}
		
		// Have changed lanes?
		// Check prior to calculating velocity
		if(lastLaneOffset != laneOffset) {
			// If start and end lane do not match we have changed lanes
			// System.out.println("Lane change detected!");
			changedLanes = true;
		}
		
		final double prevVelocity = velocity;
		
		// Update velocity
		if(pieceIndex == lastPieceIndex) {
			velocity = inPieceDistance - lastDistance; 
		} else {
			final double actual_distance = race.getTrack().get(lastPieceIndex).length(laneOffset)
					+ (changedLanes ? laneAdjust : 0.0) + inPieceDistance;
			//System.out.println("Dist Debug: " + actual_distance + ", inpiece: " +
			//		inPieceDistance + ", lastDistance: " + lastDistance);
			velocity = actual_distance - lastDistance;
			
			// Reset lane change state. Lane changes take place within pieces.
			changedLanes = false;
		}
		
		lastPieceIndex = pieceIndex;
		lastDistance = inPieceDistance;
		
		acceleration = velocity - prevVelocity;
		
		lastLaneOffset = laneOffset;
		
		// Update car angle
		
		dtheta_1 = dtheta;
		dtheta = Math.abs(angle_1 - angle);
		angle_1 = angle;
		
		theta_2 = theta_1;
		theta_1 = theta;
		theta = angle;
	}
	
	// Autogen:
	
	public String getName() {
		return name;
	}
	
	public String getColor() {
		return color;
	}
	
	public double getLength() {
		return length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getGuideFlagPosition() {
		return guideFlagPosition;
	}
	
	public double getAngle() {
		return angle;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public int getStartLane() {
		return startLane;
	}

	public int getEndLane() {
		return endLane;
	}

	public int getLap() {
		return lap;
	}
	
	public boolean isMe() {
		return me;
	}
	
	public boolean isCrashed() {
		return crashed;
	}
	
	public void setCrashed(boolean crashed) {
		this.crashed = crashed;
	}
	
	public boolean isDisqualified() {
		return disqualified;
	}
	
	public void setDisqualified(boolean disqualified) {
		this.disqualified = disqualified;
	}
	
	// Edit: name
	public boolean haveChangedLanes() {
		return changedLanes;
	}

	public double getVelocity() {
		return velocity;
	}
	
	public double getAcceleration() {
		return acceleration;
	}

	// Theta(t) is the absolute value of the car angle
	public double getTheta() {
		return theta;
	}
	
	// Theta(t-1)
	public double getTheta_1() {
		return theta_1;
	}

	// Theta(t-2)
	public double getTheta_2() {
		return theta_2;
	}
	
	// DeltaTheta(t)
	public double getDtheta() {
		return dtheta;
	}

	// DeltaTheta(t-1)
	public double getDtheta_1() {
		return dtheta_1;
	}

	@Override
	public String toString() {
		return "Car [name=" + name + ", color=" + color
				+ ", length=" + length + ", width=" + width
				+ ", guideFlagPosition=" + guideFlagPosition + ", me=" + me
				+ ", angle=" + angle + ", pieceIndex=" + pieceIndex
				+ ", inPieceDistance=" + inPieceDistance + ", startLane="
				+ startLane + ", endLane=" + endLane + ", lap=" + lap
				+ ", crashed=" + crashed + ", disqualified=" + disqualified
				+ ", laneAdjust=" + laneAdjust + ", changedLanes="
				+ changedLanes + ", velocity=" + velocity + ", theta=" + theta
				+ ", dtheta=" + dtheta + ", theta_1=" + theta_1 + ", theta_2="
				+ theta_2 + ", dtheta_1=" + dtheta_1 + "]";
	}
}
