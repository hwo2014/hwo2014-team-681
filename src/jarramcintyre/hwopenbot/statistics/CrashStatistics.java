package jarramcintyre.hwopenbot.statistics;

import java.io.PrintStream;
import java.util.ArrayList;

import jarramcintyre.hwopenbot.CurvedPiece;
import jarramcintyre.hwopenbot.Race;
import jarramcintyre.hwopenbot.StraightPiece;
import jarramcintyre.hwopenbot.TrackPiece;

public class CrashStatistics extends CrashStatisticsBase {
	private Race race;
	private boolean inCrash = false;
	
	private final ArrayList<CrashStatisticDP> dataPoints = new ArrayList<CrashStatisticDP>();
	
	public CrashStatistics(Race race) {
		this.race = race;
	}
	
	private double cummulativeCentriForce = 0;
	private double previousCentriForce = 0;
	
	public void update() {
		boolean meCrashed = race.getMe().isCrashed();
		
		if(meCrashed && !inCrash) {
			// Update previous dp as it was a crash
			inCrash = true;
			
			if(!dataPoints.isEmpty()) {
				dataPoints.get(dataPoints.size()-1).crashed = true;
			}
			
			cummulativeCentriForce = 0;
			
			return;
		}
		else if(meCrashed && inCrash) {
			return; // Do nothing. We've crashed
		}
		else if(!meCrashed && inCrash) {
			// Have just recovered from a crash. Reset statistics
			inCrash = false;
		}
		
		CrashStatisticDP dp = new CrashStatisticDP();
		dp.carThrottle = race.getLastThrottle();
		dp.theta = race.getMe().getTheta();
		dp.theta_1 = race.getMe().getTheta_1();
		dp.theta_2 = race.getMe().getTheta_2();
		dp.deltaTheta = race.getMe().getDtheta();
		dp.carVelocity = race.getMe().getVelocity();
		dp.carAcceleration = race.getMe().getAcceleration();
		
		dp.carLaneDistanceFromCenter = race.getLanes().get(race.getMe().getStartLane());
		
		TrackPiece piece = race.getTrack().get(race.getMe().getPieceIndex());
		
		if(piece instanceof StraightPiece) {
			dp.curveAngle = 0.0;
			dp.curveRadius = 0.0;
		}
		else {
			CurvedPiece cp = (CurvedPiece)piece;
			dp.curveAngle = cp.getAngle();
			// Output the radius for our lane
			dp.curveRadius = cp.getRadius(dp.carLaneDistanceFromCenter);
		}
		
		double centrifugalForce = 0;
		
		if(dp.curveRadius != 0) {
			centrifugalForce = dp.carVelocity*dp.carVelocity /  dp.curveRadius;
			cummulativeCentriForce += Math.signum(dp.curveAngle)*centrifugalForce;
		} else {
			// Decay towards zero. 0.8 is arbitrary
			cummulativeCentriForce *= 0.8;
		}
		
		dp.centrifugalForce = centrifugalForce;
		dp.cummulativeCentrifugalForce = cummulativeCentriForce;
		
		dp.deltaCentrifugalForce =  Math.abs(centrifugalForce - previousCentriForce);
		previousCentriForce = dp.deltaCentrifugalForce;
		
		dp.boost = race.usingTurbo() ? race.getTurboFactor() : 1;
		
		dataPoints.add(dp);
	}
	
	public void log(PrintStream out) {
		StringBuilder b = new StringBuilder();
		b.ensureCapacity(100); // Guestimate the final string length
		
		for(CrashStatisticDP dp : dataPoints) {
			b.append(dp.carThrottle);
			b.append(',');
			b.append(dp.theta);
			b.append(',');
			b.append(dp.theta_1);
			b.append(',');
			b.append(dp.theta_2);
			b.append(',');
			b.append(dp.deltaTheta);
			b.append(',');
			b.append(dp.carVelocity);
			b.append(',');
			b.append(dp.carAcceleration);
			b.append(',');
			b.append(dp.curveAngle);
			b.append(',');
			b.append(dp.curveRadius);
			b.append(',');
			b.append(dp.centrifugalForce);
			b.append(',');
			b.append(dp.cummulativeCentrifugalForce);
			b.append(',');
			b.append(dp.deltaCentrifugalForce);
			b.append(',');
			b.append(dp.boost);
			b.append(',');
			b.append(dp.crashed ? 1 : 0);
			b.append('\n');
			
			out.print(b.toString());
			b.setLength(0);
		}
	}
}
