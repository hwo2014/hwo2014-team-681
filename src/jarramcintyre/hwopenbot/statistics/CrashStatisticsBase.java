package jarramcintyre.hwopenbot.statistics;

import jarramcintyre.hwopenbot.Race;

import java.io.PrintStream;
import java.net.InetAddress;

public abstract class CrashStatisticsBase {
	private static final String devSystemHostname = "thor-linux";
	
	public static CrashStatisticsBase getStatsCollector(Race race) {
		/*
		try {
			String hostname = InetAddress.getLocalHost().getHostName();
			
			if(hostname.equals(devSystemHostname)) {
				return new CrashStatistics(race);
			}
		} catch(Exception e) {
			System.out.println("Error! Could not get hostname: " + e.toString());
		}
		*/
		
		return new EmptyCrashStatistics();
	}
	
	public abstract void update();
	public abstract void log(PrintStream out);
}
