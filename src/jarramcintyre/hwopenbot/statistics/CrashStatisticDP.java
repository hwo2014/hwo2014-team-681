package jarramcintyre.hwopenbot.statistics;

public class CrashStatisticDP {
	public double carThrottle;
	public double theta;
	public double theta_1;
	public double theta_2;
	public double deltaTheta;
	public double carVelocity;
	public double carAcceleration;
	public double carLaneDistanceFromCenter;
	public double curveAngle;
	public double curveRadius;
	public double centrifugalForce;
	public double cummulativeCentrifugalForce;
	public double deltaCentrifugalForce;
	public double boost;
	public boolean crashed;
}
