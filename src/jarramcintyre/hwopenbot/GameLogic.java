package jarramcintyre.hwopenbot;

import java.util.ArrayList;

import jarramcintyre.hwopenbot.messages.LeftLaneSwitch;
import jarramcintyre.hwopenbot.messages.RightLaneSwitch;
import jarramcintyre.hwopenbot.messages.SendMsg;
import jarramcintyre.hwopenbot.messages.Throttle;
import jarramcintyre.hwopenbot.messages.Turbo;

public class GameLogic {
	private Race race;
	private ArrayList<TrackPiece> track;
	private TrackUtil trackUtils;
	private Car me;
	
	private PhysicsSimulation physics;
	private PlannedVelocities[] velocityPlans;
	
	private int trackLength; // Track length in pieces for wrap-around calculations
	private double minLaneOffset = 0;
	private double maxLaneOffset = 0;
	
	public GameLogic(Race race) {
		this.race = race;
		this.track = race.getTrack();
		this.me = race.getMe();
		
		trackUtils = new TrackUtil(track);
		
		physics = new PhysicsSimulation(race,track,me);
		// One velocity plan for each lane
		velocityPlans = new PlannedVelocities[race.getLanes().size()];
		
		trackLength = track.size();
		
		// For the innermost and outermost lanes
		for(int i = 0; i < race.getLanes().size(); i++) {
			final double laneOffset = race.getLanes().get(i);
			
			if(laneOffset < minLaneOffset) {
				minLaneOffset = laneOffset;
			}
			else if(laneOffset > maxLaneOffset) {
				maxLaneOffset = laneOffset;
			}
		}
	}
	
	private void updateVelocityPlans() {
		// Have the plans been initialized
		boolean initialized = velocityPlans[0] != null;
		
		for(int i = 0; i < velocityPlans.length; i++) {
			if(initialized) {
				velocityPlans[i].recalculateMaxVelocities();
			} else {
				final double laneOffset = race.getLanes().get(i);
				velocityPlans[i] = new PlannedVelocities(physics,track,trackUtils,laneOffset);
			}
		}
	}
	
	private double distanceToNextCorner = 0.0;
	
	private boolean canChangeLane = false; // Is it possible to change lanes
	private boolean haveChangedLane = false; // Have we changed lanes on this piece?
	private int lastLaneChangeIndex = -1;
	
	private void updateStats(final int gameTick) {
		final boolean physicsModelChanged = physics.updateSlipAngle();
		
		if(physicsModelChanged) {
			updateVelocityPlans();
		}
		
		final int index = me.getPieceIndex();
		final double laneOffset = race.getLanes().get(me.getStartLane());
		
		// Find the next corner for turbo calculations
		distanceToNextCorner = 0;
		
		for(int i = index; ; i = trackUtils.nextIndex(i)) {
			final TrackPiece piece = track.get(i);
			
			if(piece instanceof CurvedPiece) {
				break;
			}
			
			distanceToNextCorner += piece.length(laneOffset);
		}
		
		
		// Update lane change info
		final TrackPiece piece = track.get(index);
		final TrackPiece nextPiece = track.get(trackUtils.nextIndex(index));
		
		canChangeLane = false;
		
		// TODO: These conditions need refining.
		if(piece.isSwitch() && me.getInPieceDistance() <= 20.0) {
			canChangeLane = true;
		}
		else if(nextPiece.isSwitch() && me.getInPieceDistance() >= 90.0) {
			canChangeLane = true;
		}
		
		if(lastLaneChangeIndex != me.getPieceIndex()) {
			lastLaneChangeIndex = -1;
			haveChangedLane = false;
		}
	}
	
	// Calculate the shortest lane for the next 10 turns
	private int shortestLane() {
		int shortestLane = 0;
		double shortestLength = -1;
		
		for(int lane = 0; lane != race.getLanes().size(); lane++) {
			final double laneOffset = race.getLanes().get(lane);
			int index = me.getPieceIndex();
			double length = 0;
			
			for(int i = 0; i < 10; i++, index = trackUtils.nextIndex(index)) {
				length += track.get(index).length(laneOffset);
			}
			
			// TODO: Consider adding a draw-breaker for length == shortestLength
			// to avoid favouring left-hand lanes (low prio)
			if(shortestLength == -1 || length < shortestLength) {
				shortestLane = lane;
				shortestLength = length;
			}
		}
		
		return shortestLane;
	}
	
	// Are we on the last lap with a straight line to the finish?
	private boolean haveClearLineToFinish() {
		// Check for straight line to finish
		if(me.getLap() >= race.getLaps() - 1) {
			for(int i = me.getPieceIndex(); i < trackLength ; i++) {
				if(track.get(i) instanceof CurvedPiece) {
					return false;
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	private double expectedVelocity = 0.0;
	
	private void updateExpectedVelocity(double throttle) {
		final double real_throttle = throttle * (race.usingTurbo() ? race.getTurboFactor() : 1.0);
		expectedVelocity = physics.velocity(me.getVelocity(),1,real_throttle);
	}
	
	// Is the lane being blocked by someone slower than us
	private boolean beingBlocked() {
		// 1.03 allows some margin for error to avoid silly lane changes
		if(me.getVelocity() * 1.03 < expectedVelocity) {
			// Is there a car in our lane in front of us?
			for(Car car : race.getCars()) {
				if(car.isMe()) {
					continue;
				}
				
				// In the same lane and on the same piece or the piece in front
				if(car.getStartLane() == me.getStartLane() &&
					(car.getPieceIndex() == me.getPieceIndex() ||
					 car.getPieceIndex() == trackUtils.nextIndex(me.getPieceIndex()))) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	// Generate a message to switch to an inner lane
	private SendMsg generateLaneSwitchMessage(final int targetLane, final int gameTick) {
		haveChangedLane = true;
		lastLaneChangeIndex = (me.getInPieceDistance() < 20) ?
				me.getPieceIndex() : trackUtils.nextIndex(me.getPieceIndex());
		
		// Turn left or right?
		boolean left = targetLane < me.getStartLane();
		
		// If we are already on the target lane then we must be being blocked.
		// Switch to another lane..
		if(targetLane == me.getStartLane()) {
			// TODO: Decided to favour the right hand lane here...
			// Should check lengths again
			
			int toSwitch = race.getLanes().size() - 1; 
			// If on the rightmost go left. Otherwise go right
			left = toSwitch == me.getStartLane();
		}
		
		if(left) {
			System.out.println("To the left lane! Piece: " + me.getPieceIndex());
			return new LeftLaneSwitch(gameTick);
		} else {
			System.out.println("To the right lane! Piece: " + me.getPieceIndex());
			return new RightLaneSwitch(gameTick);
		}
	}
	
	private final double minimumVelocity = 3.0; // No point going bellow this
	private final double minimumTurboDistance = 350; // Don't turbo unless there are more than 3 straight pieces
	private final double maximumSlipAngle = 50; // Don't allow slip above 50 degrees
	private final double maximumLaneChangeSlipAngle = 25; // Don't change lanes while slipping (too much)
	
	public SendMsg doRound(final int gameTick) {
		final double velocity = me.getVelocity();
		
		updateStats(gameTick);
		
		// Initial constants calculations		
		if(gameTick <= 3) {
			// Collect initial-info to calculate physics constants
			if(gameTick > 0) {
				physics.initialVelocities[gameTick-1] = velocity;
			}
			
			updateExpectedVelocity(race.getLastThrottle());
			
			printUpdates(gameTick,"Full steam ahead",1.0);
			race.setLastThrottle(1.0);
			return new Throttle(1.0,gameTick);
		}
		else if(gameTick == 4) {
			// Calculate physics constants on fourth turn
			physics.calculateConstants();
			updateVelocityPlans();
			
			updateExpectedVelocity(race.getLastThrottle());
			
			printUpdates(gameTick,"Full steam ahead",1.0);
			race.setLastThrottle(1.0);
			return new Throttle(1.0,gameTick);
		}
		
		// No need to ever go less than minimumVelocity.
		// A velocity less than minimumVelocity suggest a logic error (e.g. unstable equ)
		if(velocity <= minimumVelocity) {
			// Catch-all. Any speed less than this and throttle should be full!
			
			updateExpectedVelocity(race.getLastThrottle());
			
			printUpdates(gameTick,"Full steam ahead",1.0);
			race.setLastThrottle(1.0);
			return new Throttle(1.0,gameTick);
		}
		
		// Basic position info shared over the next set of calculations
		final int lane = me.getEndLane();
		final double laneOffset = race.getLanes().get(lane);
		final int pieceIndex = me.getPieceIndex();
		final TrackPiece piece = track.get(pieceIndex);
		
		// Break hard if we are slipping too much
		if(Math.abs(me.getAngle()) > maximumSlipAngle) {
			System.out.println("Is curve piece: " + (piece instanceof CurvedPiece));
			printUpdates(gameTick,"We're going out of control! Max: " + maximumSlipAngle,0.0);
			race.setLastThrottle(0.0);
			return new Throttle(0.0,gameTick);
		}
		
		// Calculate throttle
			
		double targetVelocity = velocityPlans[lane].maxVelocities[pieceIndex];
		
		// Find the smallest target velocity coming up
		double smallestTargetVelocity = targetVelocity;
		double smallestDistance = piece.length(laneOffset) - me.getInPieceDistance();
		
		// Search for the smallest upcoming target velocity.
		// Safety valve: Never need to search past ourselves.
		for(int i = trackUtils.nextIndex(pieceIndex); i != pieceIndex; i = trackUtils.nextIndex(i)) {
			final double pieceVelocity = velocityPlans[lane].maxVelocities[i];
			
			if(pieceVelocity > smallestTargetVelocity) {
				break;
			}
			
			smallestTargetVelocity = pieceVelocity;
			smallestDistance += track.get(i).length(laneOffset);
		}
		
		if(smallestTargetVelocity != targetVelocity) {
			targetVelocity = Math.min(targetVelocity,
					physics.minimumVelocityToBreak(smallestDistance, smallestTargetVelocity));
		}
		
		// Go at max speed possible for target velocity
		double throttle = clampThrottle(physics.maximumThrottle(velocity,targetVelocity));
			
		// Correct the throttle if currently in turbo mode
		if(targetVelocity < me.getVelocity() && race.usingTurbo()) {
			throttle /= race.getTurboFactor();
		}
		
		// If we have turbo and should turbo, then do it
		// (this is here because the calculation uses the throttle value)
		if(race.haveTurbo()) {
			boolean shouldTurbo = distanceToNextCorner > minimumTurboDistance;
			shouldTurbo &= throttle == 1.0;
			shouldTurbo |= haveClearLineToFinish();
				
			if(shouldTurbo) {
				System.out.println("Turbo Debug: ClearLine: " + haveClearLineToFinish() +
						", Next Corner: " + distanceToNextCorner);
					
				race.useTurbo(); // Mark our turbo as used! Do this before velocity updates
				updateExpectedVelocity(race.getLastThrottle());
					
				printUpdates(gameTick,"Turbo",race.getLastThrottle());
				return new Turbo(gameTick);
			}
		}
		
		// If we can switch lanes, and should switch lanes, then do it
		// (this is here because the calculation uses the throttle value)
		if(canChangeLane && !haveChangedLane) {
			// Shortest lane for the next 10 pieces
			final int shortestLane = shortestLane();
			
			// Only change if we are not on the shortest lane for the next 10 pieces
			// or we are being blocked
			boolean shouldSwitchLanes = (shortestLane != me.getStartLane()) || beingBlocked();
			// Prefer to break than change lanes 
			shouldSwitchLanes &= race.getLastThrottle() <= throttle;
			// Don't change lanes when slipping
			shouldSwitchLanes &= Math.abs(me.getAngle()) <= maximumLaneChangeSlipAngle;
			// Do not switch lanes before calculating the
			// necessary physics constants. I.e. getting slip data!
			shouldSwitchLanes &= physics.haveSlipConstants();
			
			if(shouldSwitchLanes) {
				updateExpectedVelocity(race.getLastThrottle());
				return generateLaneSwitchMessage(shortestLane,gameTick);
			}
		}
			
		// Otherwise just throttle
		printUpdates(gameTick, "Throttle. Target Velocity: " + targetVelocity, throttle);
		race.setLastThrottle(throttle);
		return new Throttle(throttle,gameTick);
	}
	
	public void updateCrash(Car car) {
		// The car crashed. Update the physics model if needed
		final double velocity = car.getVelocity(); // Velocity should not be updated at this stage
		
		final double laneOffset = race.getLanes().get(car.getStartLane());
		final TrackPiece piece = track.get(car.getPieceIndex());
		
		if(piece instanceof StraightPiece) {
			// Crashes can happen on straight turns due to raming
			// No need to update in this situation
			return;
		}
		
		final double r = ((CurvedPiece)piece).getRadius(laneOffset);
		
		System.out.println("Crash observed for: " + car.getName() + " (" + car.getColor() + ") "
				+ " v=" + velocity + ", r=" + r);
		
		final boolean physicsModelChanged = physics.calculateMaxCentrifugalForce(velocity, r, car.isMe());
		
		if(physicsModelChanged) {
			updateVelocityPlans();
		}
	}
	
	// Util:
	
	private void printUpdates(final int gameTick, final String msg, final double throttle) {
		//System.out.println("Tick: " + gameTick + ": " + msg + " -> Velocity: " + me.getVelocity()
		//		+ ", Theta: " + me.getTheta() + ", DTheta: " + me.getDtheta() + " -> " + throttle);
	}
	
	private double clampThrottle(double throttle) {
		if(throttle < 0) {
			return 0;
		} else if(throttle > 1) {
			return 1;
		} else if(Double.isInfinite(throttle) || Double.isNaN(throttle)) {
			return 0; // Be on the safe side
		} else {
			return throttle;
		}
	}
}

class PhysicsSimulation {
	private Race race;
	private ArrayList<TrackPiece> track;
	private Car me;
	
	public PhysicsSimulation(final Race race, final ArrayList<TrackPiece> track, final Car me) {
		this.race = race;
		this.track = track;
		this.me = me;
	}
	
	// Velocity related calculations:
	
	public final double initialVelocities[] = new double[3];
	public double drag = 1.0; // Will be overriden later
	public double mass = 1.0;
		
	public void calculateConstants() {
		final double v1 = initialVelocities[0];
		final double v2 = initialVelocities[1];
		final double v3 = initialVelocities[2];
			
		drag = (v1 - (v2 - v1)) / (v1*v1);
			
		final double k = drag; // Keep the notation consistent
			
		mass = 1.0 / (Math.log((v3 - (1 / k)) / (v2 - (1 / k))) / (-k) );
			
		System.out.println("Constants: k (drag): " + k + ", m (mass): " + mass);
	}
		
	// Velocity in ticks time given the current velocity (v) and throttle
	public double velocity(final double v, final int ticks, final double throttle) {
		// Keep the notation consistent
		final double h = throttle;
		final double k = drag;
		final double m = mass;
		final double t = (double)ticks;
		
		return (v - (h/k)) * Math.exp((-k/t) / m) + (h/k);
	}
		
	// Ticks to reach velocity (v) given current velocity (v0) and throttle
	public double ticksToReachVelocity(final double v, final double v0, final double throttle) {
		// Consistent notation
		final double h = throttle;
		final double k = drag;
		final double m = mass;
		
		final double t = Math.log((v - (h/k)) / (v0 - (h/k))) * m/(-k);
			
		return Math.ceil(t);
	}
		
	// Distance traveled in ticks given current velocity (v) and current throttle
	public double distanceInTicks(final int ticks, final double v, final double throttle) {
		// Consistent notation
		final double t = (double)ticks;
		final double h = throttle;
		final double k = drag;
		final double m = mass;
		
		return (m / k) * (v - (h / k)) * (1.0 - Math.exp((-k*t) / m)) + (h/k)*t;
	}
		
	// Distance to reach target_v from current_v with 0 throttle
	public double minimumBreakingDistance(final double current_v, final double target_v) {
		// Consistent notation
		final double v0 = current_v;
		final double v1 = target_v;
		final double k = drag;
		final double m = mass;
		
		// TODO: s = (m/k)*(v0 - v1)
		final double s = v0*(m/k)*(1 - (v1/v0));
			
		// Be on the safe side
		return s; // *1.05
	}
	
	// Starting velocity required to reach target_v in distance
	public double minimumVelocityToBreak(final double distance, final double target_v) {
		// Consistent notation
		final double s = distance;
		final double v1 = target_v;
		final double k = drag;
		final double m = mass;
		
		final double v0 = s*(k / m) + v1;
		
		// Be on the safe side
		return v0; // *0.98
	}
	
	// Throttle required to reach target_v given current_v in the shortest time possible
	public double maximumThrottle(final double current_v, final double target_v) {
		// Consistent notation:
		final double v0 = current_v;
		final double v1 = target_v;
		final double k = drag;
		final double m = mass;
		final double expkm = Math.exp(-k / m);
			
		final double calcValue = (v1 - expkm*v0) * k / (1.0 - expkm);
		
		// Be on the safe side
		return calcValue; // *0.98
	}
		
	// Centrifugal force / Slip Angle
	
	private boolean gotSlipAngle = false;
			
	public boolean updateSlipAngle() {
		// Calculate the slip angle when we are first on a curved piece and slipping
		// Only need to calculate once
		if(!gotSlipAngle && Math.abs(me.getAngle()) > 0 &&
				track.get(me.getPieceIndex()) instanceof CurvedPiece) {
			final double laneOffset = race.getLanes().get(me.getStartLane());
			final double r = ((CurvedPiece)track.get(me.getPieceIndex())).getRadius(laneOffset);
			final double v = me.getVelocity();
			
			// omega = (v / r) (radians)
			final double angularVelocity = (v / r) * 180 / Math.PI;
			// delta-angle = Angle - 0 (were not rotating previously..)
			final double maxToSlip = angularVelocity - me.getAngle();
			
			final double freshhold_v = (maxToSlip*Math.PI / 180) * r;
					
			final double maxCentrifugalForce = freshhold_v*freshhold_v / r;
					
			System.out.println("Calculated angular-velocity: " + angularVelocity +
					" -> Centri: " + maxCentrifugalForce);
					
			// Up this slightly as the actually result tends to be too conservative...
			//observedMaxCentrifugalForce = Math.min(maxCentrifugalForce*1.2,observedMaxCentrifugalForce);
			// Cap the value to avoid risk of calculation overflow. In practice it shouldn't be over 0.46
			observedMaxCentrifugalForce = Math.min(maxCentrifugalForce, 0.46);
					
			gotSlipAngle = true;
			return true;
		}
		
		return false;
	}
	
	public boolean haveSlipConstants() {
		return gotSlipAngle;
	}
	
		
	private final double estMaxCentrifugalForce = 0.42; // Generally around 0.46-0.47
	private double observedMaxCentrifugalForce = estMaxCentrifugalForce;
		
	public boolean calculateMaxCentrifugalForce(final double v, final double r, boolean was_me) {
		final double c = (v*v) / r;
		final double currentMax = observedMaxCentrifugalForce;
			
		if(was_me && c > observedMaxCentrifugalForce) {
			// In this case observedMaxCentrifugalForce is likey wrong
			// Bring it down somewhat.
			observedMaxCentrifugalForce = observedMaxCentrifugalForce * 0.93;
		}
		if(observedMaxCentrifugalForce == c) {
			// Drop the max centrifugal force slightly as it is clearly wrong!
			observedMaxCentrifugalForce = c * 0.98;
		}
		else {
			// Only revise down not up. The velocity may have been way over the max
			observedMaxCentrifugalForce = Math.min(c, observedMaxCentrifugalForce);
		}
			
		System.out.println("Calculated max centrifugal force: " + observedMaxCentrifugalForce);
		
		return currentMax != observedMaxCentrifugalForce;
	}
		
	public double maxVelocityForCorner(final double r) {
		final double c = observedMaxCentrifugalForce;
		return Math.sqrt(c*r);
	}
	
	public double maxVelocityForCorner(final double r, final double difficultyModifier) {
		final double c = observedMaxCentrifugalForce * difficultyModifier;
		return Math.sqrt(c*r);
	}
}

class PlannedVelocities {
	private PhysicsSimulation physics = null;
	private ArrayList<TrackPiece> track = null;
	private TrackUtil trackUtils = null;
	
	public double[] maxVelocities = null;
	public double[] cornerDifficulties = null;
	public double laneOffset = 0;
	
	public PlannedVelocities(PhysicsSimulation physics, ArrayList<TrackPiece> track, TrackUtil trackUtils,
			double laneOffset)
	{
		this.physics = physics;
		this.track = track;
		this.trackUtils = trackUtils;
		this.maxVelocities = new double[track.size()];
		this.cornerDifficulties = new double[track.size()];
		this.laneOffset = laneOffset;
		recalculateMaxVelocities();
	}
	
	public void recalculateMaxVelocities()
	{
		// Populate with initial estimates
		
		double previousCornerAngle = 0; // -1, 0 (straight), 1
		double curveDifficulty = 0;
		
		for(int i = 0; i < track.size(); i++) {
			final TrackPiece piece = track.get(i);
			
			if(piece instanceof StraightPiece) {
				// Allowed velocity
				maxVelocities[i] = 100;
				
				// Difficulty
				previousCornerAngle = 0;
				cornerDifficulties[i] = 0;
			} else {
				final CurvedPiece curve = (CurvedPiece)piece;
				
				// Difficulty
				final double cornerAngle = curve.getAngle();
				
				if(cornerAngle*previousCornerAngle < 1) {
					previousCornerAngle = cornerAngle;
					
					// New curve so recalculate difficulty
					double minimumRadius = 100000000; // Start with an impossible value
					double length = 0;
					
					for(int j = i; ; j = trackUtils.nextIndex(j)) {
						final TrackPiece curPiece = track.get(j);
						
						if(curPiece instanceof StraightPiece) {
							break;
						}
						
						CurvedPiece curCurve = (CurvedPiece)curPiece;
						
						// Curve goes in opposite direction
						if(curCurve.getAngle() * cornerAngle < 1) {
							break;
						}
						
						length += curCurve.length(laneOffset);
						minimumRadius = Math.min(minimumRadius,curCurve.getRadius(laneOffset));
					}
					
					System.out.println("Length: " + length + " Minimum Radius: " + minimumRadius);
					
					// Calculate difficulty based on length and radius
					
					curveDifficulty = 1.0;
					
					if(minimumRadius > 150) {
						curveDifficulty = 1.3; // T'is easy
					}
					else if(length / minimumRadius >= 2.35) {
						// 2.35 ~ 0.75pi.
						//curveDifficulty = 0.95; // Take it easy!
						curveDifficulty = 0.95;
					}
					else if(length / minimumRadius >= 2.1) {
						curveDifficulty = 1.05;
					}
					else if(length / minimumRadius >= 1.5) {
						curveDifficulty = 1.1;
					}
					else if(length / minimumRadius >= 1.0) {
						curveDifficulty = 1.2;
					} else {
						curveDifficulty = 1.3;
					}
				}
				
				cornerDifficulties[i] = curveDifficulty;
				
				// Allowed velocity
				final double r = curve.getRadius(laneOffset);
				maxVelocities[i] = physics.maxVelocityForCorner(r, curveDifficulty);
				
			}
		}
		
		// Refine estimates until there is nothing to update
		
		// TODO: Optimise me by searching backwards from the minimum velocity
		// (low prio - generally finish in 0-1ms)
		final long startTime = System.currentTimeMillis();
		
		// The system should settle before size^2 iterations
		// But cut off at size^2 iterations to be safe
		final int size = track.size();
		final int maxIterations = track.size() * track.size();
		int totalIterations = 0;
		int iterationsSinceLastUpdate = 0;
		int i = 1, prev_i = 0;
		
		while(iterationsSinceLastUpdate < size && totalIterations < maxIterations) {
			if(maxVelocities[i] < maxVelocities[prev_i]) {
				final double currentVel = maxVelocities[prev_i];
				final double targetVel = maxVelocities[i];
				// Make sure we can break on time for this piece
				final double length = track.get(i).length(laneOffset);
				final double breakingDistance = physics.minimumBreakingDistance(currentVel,targetVel);
				
				if(breakingDistance > length) {
					// Need to take the piece at prev_i slower
					maxVelocities[prev_i] = physics.minimumVelocityToBreak(length, targetVel);
					iterationsSinceLastUpdate = 0;
				}
			}
			
			prev_i = i;
			i = trackUtils.nextIndex(i);
			
			totalIterations++;
			iterationsSinceLastUpdate++;
		}
		
		System.out.println("Calculating maximum velocities took: " + (System.currentTimeMillis() - startTime));
		
		System.out.println("Plan for laneOffset: " + laneOffset);
		StringBuilder plan = new StringBuilder();
		StringBuilder difficulties = new StringBuilder();
		
		for(int j = 0; j < maxVelocities.length; j++) {
			plan.append(maxVelocities[j]);
			plan.append(", ");
			difficulties.append(cornerDifficulties[j]);
			difficulties.append(", ");
		}
		
		System.out.println(plan.toString());
		System.out.println(difficulties.toString());
	}
}

class TrackUtil {
	private int trackLength;
	
	public TrackUtil(ArrayList<TrackPiece> track) {
		trackLength = track.size();
	}
	
	public int nextIndex(int i) {
		return i + 1 < trackLength ? i + 1 : 0;
	}
}
