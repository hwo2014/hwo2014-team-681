package jarramcintyre.hwopenbot.messages;

import com.google.gson.Gson;

public abstract class SendMsg {
	private int gameTick;
	
	public SendMsg(int gameTick) {
		this.gameTick = gameTick;
	}
	
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }
    
    protected int gameTick() {
    	return gameTick;
    }

    protected abstract String msgType();
}