package jarramcintyre.hwopenbot.messages;

public class Turbo extends SendMsg {
	public Turbo(int gameTick) {
		super(gameTick);
	}
	
	@Override
	protected Object msgData() {
		return "Lolz I got turbo";
	}
	
	@Override
	protected String msgType() {
		return "turbo";
	}

}
