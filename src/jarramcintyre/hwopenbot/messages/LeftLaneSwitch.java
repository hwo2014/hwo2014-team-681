package jarramcintyre.hwopenbot.messages;

public class LeftLaneSwitch extends SendMsg {
	public LeftLaneSwitch(int gameTick) {
		super(gameTick);
	}
	
	@Override
	protected Object msgData() {
		return "Left";
	}
	
	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "switchLane";
	}

}
