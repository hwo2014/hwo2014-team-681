package jarramcintyre.hwopenbot.messages;

public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value, int gameTick) {
    	super(gameTick);
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}