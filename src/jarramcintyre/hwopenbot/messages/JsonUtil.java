package jarramcintyre.hwopenbot.messages;

import java.util.AbstractMap;

public class JsonUtil {
    @SuppressWarnings("unchecked")
	public static Object getValue(Object value, String key) {
    	AbstractMap<String,?> o = (AbstractMap<String,?>)value;
    	
    	String subKeys[] = key.split(":");
    	
    	for(int i = 0; i < subKeys.length-1; i++) {
    		if(o == null) {
    			return null;
    		}
    		
    		o = (AbstractMap<String,?>)o.get(subKeys[i]);
    	}
    	
    	return o.get(subKeys[subKeys.length-1]);
    }
}
