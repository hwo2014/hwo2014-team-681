package jarramcintyre.hwopenbot.messages;

public class RightLaneSwitch extends SendMsg {
	public RightLaneSwitch(int gameTick) {
		super(gameTick);
	}
	
	@Override
	protected Object msgData() {
		return "Right";
	}
	
	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "switchLane";
	}

}
