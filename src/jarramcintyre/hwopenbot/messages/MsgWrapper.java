package jarramcintyre.hwopenbot.messages;

public class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    MsgWrapper(final String msgType, final Object data, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
    }
    
    public Object getValue(String key) {
    	return JsonUtil.getValue(data, key);
    }
}