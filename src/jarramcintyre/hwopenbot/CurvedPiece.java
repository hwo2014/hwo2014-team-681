package jarramcintyre.hwopenbot;

public class CurvedPiece extends TrackPiece {
	private double radius;
	private double angle;
	
	// Autogen:
	
	public CurvedPiece(double radius, double angle) {
		super();
		this.radius = radius;
		this.angle = angle;
	}
	
	@Override
	public double length(double laneOffset) {
		double ar = Math.abs(angle * Math.PI / 180);
		double rad = radius + (angle > 0 ? - laneOffset : laneOffset);
		return ar*rad;
	}
	
	// Autogen:

	public double getRadius() {
		return radius;
	}
	
	public double getRadius(double laneOffset) {
		return radius + (angle > 0 ? - laneOffset : laneOffset);
	}

	public double getAngle() {
		return angle;
	}

	@Override
	public String toString() {
		return "CurvedPiece [radius=" + radius + ", angle=" + angle + ", isSwitch=" + isSwitch() + "]";
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(angle);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(radius);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
