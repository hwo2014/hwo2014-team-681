package jarramcintyre.hwopenbot;

import jarramcintyre.hwopenbot.messages.MsgWrapper;
import jarramcintyre.hwopenbot.messages.SendMsg;
import jarramcintyre.hwopenbot.statistics.CrashStatisticsBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.google.gson.Gson;

enum GameState {
	StartingRace,
	InRace,
	FinishedRace
}

public class Main {
    public static void main(String... args) throws UnknownHostException, IOException {    	
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		
		try {
			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
			new Main(reader, writer, botName, botKey);
		}
		finally {
			// Be good
			socket.close();
		}
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, String botName, String botKey)
    		throws IOException {
        this.writer = writer;
        String line = null;

        
        send("{\"msgType\": \"join\",  \"data\": {\"name\": \""+ botName
        		+ "\", \"key\": \"" + botKey + "\"}}");
         
        /*
        final String track = "suzuka";
        final int cars = 1;
        
        send(
        	"{\"msgType\": \"createRace\", \"data\": {\"botId\": {\"name\": \"" + botName +
        	"\",\"key\": \"" + botKey + "\"},\"trackName\": \"" +  track + "\",\"carCount\": " + cars + "}}"
        );
        */
		
        String myName = "WashikiGoushuujin";
        String myColor = null;
        Race race = null;
        
        GameLogic logic = null;
        CrashStatisticsBase stats = null;
        
        GameState state = GameState.StartingRace;
        
        long previousMs = System.currentTimeMillis();
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			// System.out.println(line);
			
			if(state == GameState.StartingRace) {
				if(msgFromServer.msgType.equals("join")) {
					System.out.println("Joined");
				}
				else if(msgFromServer.msgType.equals("yourCar")) {
					myName = (String)msgFromServer.getValue("name");
					myColor = (String)msgFromServer.getValue("color");
					System.out.println("I am: " + myName + " color: " + myColor);
				}
				else if(msgFromServer.msgType.equals("gameInit")) {
					assert(race == null);
					assert(logic == null);
					assert(stats == null);
					
					race = new Race(myName,myColor,msgFromServer);
					logic = new GameLogic(race);
					stats = CrashStatisticsBase.getStatsCollector(race);
					
					System.out.println("Race initiated: " + race.toString());
				}
				else if(msgFromServer.msgType.equals("gameStart")) {
					assert(race != null);
					assert(logic != null);
					assert(stats != null);
					
					state = GameState.InRace;
					
					// Do the first round
					send(logic.doRound(0));
					stats.update();
					
					System.out.println("Time to first round: " + (System.currentTimeMillis() - previousMs));
					previousMs = System.currentTimeMillis();
				}
				// I guess this comes here...
				else if(msgFromServer.msgType.equals("tournamentEnd")) {
					state = GameState.FinishedRace;
				}
			}
			else if(state == GameState.InRace) {				
				if(msgFromServer.msgType.equals("carPositions")) {
					int gameTick = msgFromServer.gameTick;
					
					race.updateCars(msgFromServer);
					race.tickTurbo();
					
					// Update once we've received car positions
					send(logic.doRound(gameTick));
					stats.update();
					
					//System.out.println("Time to do round: " + gameTick + " was: "
					//				+ (System.currentTimeMillis() - previousMs));
					previousMs = System.currentTimeMillis();
				}
				else if(msgFromServer.msgType.equals("crash")) {
					Car car = race.updateCrash(msgFromServer);
					logic.updateCrash(car); // Observe the crash before the car stats are updated 
				}
				else if(msgFromServer.msgType.equals("spawn")) {
					race.updateSpawn(msgFromServer);
				}
				else if(msgFromServer.msgType.equals("turboAvailable")) {
					race.updateTurbo(msgFromServer);
				}
				else if(msgFromServer.msgType.equals("lapFinished")) {
					// Oh no what should we do...
					send("{\"msgType\": \"ping\"}");
				}
				else if(msgFromServer.msgType.equals("finish")) {
					// Oh no what should we do...
					send("{\"msgType\": \"ping\"}");
				}
				else if(msgFromServer.msgType.equals("dnf")) {
					race.updateDnf(msgFromServer);
					send("{\"msgType\": \"ping\"}");
				
					if(race.getMe().isDisqualified()) {
						System.out.println(line);
						break; // Oh well not much to do at this point...
					}
				}
				else if (msgFromServer.msgType.equals("gameEnd")) {
					race = null;
					logic = null;
					
					stats.log(System.out);
					stats = null;
					
					state = GameState.StartingRace;
				}
				else {
					send("{\"msgType\": \"ping\"}");
				}
			}
			else {
				System.out.println("Message when in finished state: " + line);
			}
        }
        
        if(stats != null) {
        	System.out.println("Crash statistics: ");
        	stats.log(System.out);
        }
    }
    
    private void send(final String msg) {
    	writer.println(msg);
    	writer.flush();
    }

    private void send(final SendMsg msg) {
        send(msg.toJson());
    }
} 