package jarramcintyre.hwopenbot;

public class StraightPiece extends TrackPiece {
	private double length;

	// Autogen: 
	
	public StraightPiece(double length) {
		super();
		this.length = length;
	}
	
	@Override
	public double length(double laneoffset /* Ignore for straight pieces*/) {
		return length;
	}

	@Override
	public String toString() {
		return "StraightPiece [length=" + length + ", isSwitch=" + isSwitch()
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(length);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
