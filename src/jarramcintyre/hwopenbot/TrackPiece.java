package jarramcintyre.hwopenbot;

import java.util.AbstractMap;

public abstract class TrackPiece {
	private boolean isSwitch = false;
	
	public static TrackPiece createPiece(AbstractMap<String,?> piece) {
		TrackPiece tp;
		
		if(piece.containsKey("length")) {
			tp = new StraightPiece(
				((Double)piece.get("length")).doubleValue()
			);
		}
		else if(piece.containsKey("radius")) {
			tp = new CurvedPiece(
				((Double)piece.get("radius")).doubleValue(),
				((Double)piece.get("angle")).doubleValue()
			);
		}
		else {
			throw new IllegalArgumentException("Cannot parse: " + piece.toString());
		}
		
		tp.isSwitch = piece.containsKey("switch") && ((Boolean)piece.get("switch")).booleanValue();
		
		return tp;
	}
	
	public boolean isSwitch() {
		return isSwitch;
	}
	
	public abstract double length(double laneDistance);
	
	// Autogen:

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isSwitch ? 1231 : 1237);
		return result;
	}
}
